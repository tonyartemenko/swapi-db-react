import React, {Component} from "react";
import ErrorIndicator from "../error-indicator";

//TODO: create pater component ErrorBoundary for errors in inside component
// https://ru.reactjs.org/docs/error-boundaries.html

export default class ErrorBoundary extends Component{

    state = {
        hasError: false
    }

    componentDidCatch(error, info) {

        this.setState({
            hasError: true
        });
    }


    render() {
        if (this.state.hasError) {
            return <ErrorIndicator />;
        }

        return this.props.children
    }
}