import React, {Component} from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import PeoplePage from '../people-page';
import ErrorButton from '../error-button';
import ErrorIndicator from '../error-indicator';

import './app.css';
import ItemList from "../item-list";
import Row from "../row";
import ErrorBoundary from "../error-boundary";
import ItemDetails, { Record } from "../item-details/item-details";
import SwapiService from "../../services/swapi-service";


export default class App extends Component{

    state = {
        showRandomPlanet: true,
        hasError: false
    };

    swapiService = new SwapiService();

    toggleRandomPlanet = () => {
      this.setState((state) => {
          return {
              showRandomPlanet: !state.showRandomPlanet
          }
      })
    };

    componentDidCatch() {
        this.setState({ hasError: true });
    }

    render() {
        const { getPerson, getStarship, getPersonImage, getStarshipImage } = this.swapiService;

        const personDetails = (
            <ItemDetails
                itemId={11}
                getData={getPerson}
                getImageUrl={getPersonImage} >

                <Record field="gender" label="Gender" />
                <Record field="eyeColor" label="Eye Color" />

            </ItemDetails>
        );

        const starshipDetails = (
            <ItemDetails
                itemId={5}
                getData={getStarship}
                getImageUrl={getStarshipImage}>

                <Record field="model" label="Model" />
                <Record field="length" label="Length" />
                <Record field="costInCredits" label="Cost" />
            </ItemDetails>
        );

        if (this.state.hasError) {
            return <ErrorIndicator />
        }

        return (
            <div className='container'>
                <Header />
                {
                    this.state.showRandomPlanet ?
                        <RandomPlanet />
                        : null
                }

                {/*<button*/}
                {/*    className="toggle-planet btn btn-warning btn-md"*/}
                {/*    onClick={this.toggleRandomPlanet}>*/}
                {/*   Toggle random planet*/}
                {/*</button>*/}
                {/*<ErrorButton />*/}

                {/*<PeoplePage />*/}

                <Row
                    left={personDetails}
                    right={starshipDetails} />

            </div>
        );
    }
};