import React, {Component} from 'react';
import SwapiService from '../../services/swapi-service';
import ItemList from '../item-list/item-list';
import ItemDetails from '../item-details';
import ErrorIndicator from '../error-indicator/error-indicator';
import Row from '../row/row'
import ErrorBoundary from "../error-boundary";


export default class PeoplePage extends Component {
    state = {
        selectedPerson: null,
    };

    swapiService = new SwapiService();

    onePersonSelected = (selectedPerson) => {
        this.setState({
            selectedPerson: selectedPerson
        })
    };


    render() {
        const itemList = (
            <ItemList
                onItemSelected={this.onePersonSelected}
                getData={this.swapiService.getAllPeople}

// TODO: (l73) used pattern react render function renderFunc={({}) => <p> content <p/>}
//                 renderItem={({name, gender, birthYear}) => `${name} / ${gender} / ${birthYear}`}
            >
                {(item) => (
                    `${item.name} (${item.birthYear})`
                )}
            </ItemList>
        );

        const personDetails = (
            <ErrorBoundary>
                <ItemDetails
                    personId={this.state.selectedPerson}
                />
            </ErrorBoundary>
        );

        return (
            <ErrorBoundary>
                <Row left={itemList} right={personDetails} />
            </ErrorBoundary>
        )
    }
}